using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NVIDIA;

public class Attracted : MonoBehaviour
{
    private NVIDIA.Flex.FlexClothActor FlexComponet;
    public GameObject Attractor;
    public float mul = 1;
    void Start()
    {
        FlexComponet = this.GetComponent<NVIDIA.Flex.FlexClothActor>();
        
        
        Vector3 vel = (Attractor.transform.position - this.transform.position);
        Vector3 input = vel;
        FlexComponet.ApplyImpulse(input*mul*50.0f);
    }
    
    
    void Update()
    {
        
        Vector3 vel = (Attractor.transform.position - this.transform.position);
        Vector3 input = vel;
        FlexComponet.ApplyImpulse(input*mul);
     
    }
}
