using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NVIDIA;
using UnityEngine.SceneManagement;

public class ResetPositions : MonoBehaviour
{
    private NVIDIA.Flex.FlexClothActor FlexComponet;
    Vector3 startPos;
    // Start is called before the first frame update
    void Start()
    {
        FlexComponet = this.GetComponent<NVIDIA.Flex.FlexClothActor>();
        startPos=transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
