using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    public float dist = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 p = new Vector3();

        p.x += Mathf.Sin(Time.time) * dist;
        p.y = transform.position.y;
        p.z += Mathf.Cos(Time.time) * dist;

        transform.position = p;
    }
}
